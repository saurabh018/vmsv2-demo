<?php
  
class BlogController extends BaseController
{
    public function getRecord()
    {
        $blogModel = new Blog;
        $blogModel->setConnection('mysql2');
        $find = $blogModel->find(1);
        return $find;
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['name', 'url', 'title', 'content'];
    protected $table = 'pages';
}

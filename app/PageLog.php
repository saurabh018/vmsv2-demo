<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageLog extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'log_created', 'action', 'user_id', 'table_id', 'revision_id',];
    protected $table = 'page_logs';
}

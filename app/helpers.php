<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

function logCreate($table = "", $data){
    $schema = Schema::hasTable($table.'_logs');
    $logs = $table.'_logs';
    
    if ($schema == 1) {
        
    }else{
         Schema::create($table.'_logs', function (Blueprint $table) {
            $table->id();
            $table->string('action');
            $table->integer('user_id');
            $table->integer('revision_id');
            $table->timestamps();
        });
    }
   
}
   
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('action');
            $table->integer('user_id');
            $table->integer('revision_id');
            $table->integer('table_id')->comment('page table_id_pk');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->dateTime('log_created')->comment('history created datetime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_logs');
    }
}
